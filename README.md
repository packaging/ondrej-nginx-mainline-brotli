# [NGINX](https://launchpad.net/~ondrej/+archive/ubuntu/nginx-mainline) with [brotli](https://github.com/google/ngx_brotli) compression

> **PLEASE READ**: If you like Ondřej Surýs work and want to give him a little motivation, please consider donating: https://donate.sury.org !

This repo just applies 2 patches to add brotli compression module to Ondřej Surýs nginx builds. I just figured out how to apply patches to Ondřejs builds and wired things up in a script and patch files.

All credits belong to:

* [nginx](http://nginx.org/) for nginx obviously
* [Ondřej Surý](https://donate.sury.org) for his packaging efforts for quite some time now
* [Google](https://github.com/google/ngx_brotli) for brotli and the nginx module

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).

## Builds

* Ubuntu Bionic
* Ubuntu Focal
* Debian Stretch
* Debian Buster

## Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-ondrej-nginx-mainline-brotli.asc https://packaging.gitlab.io/ondrej-nginx-mainline-brotli/gpg.key
```

## Add repo to apt

```bash
apt-get install lsb-release apt-transport-https
echo "deb [arch=amd64] https://packaging.gitlab.io/ondrej-nginx-mainline-brotli $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/morph027-ondrej-nginx-mainline-brotli.list
```
