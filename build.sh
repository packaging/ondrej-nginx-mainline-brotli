#!/bin/bash

set -e

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"


printf "\e[0;36mInstalling build toolchain ...\e[0m\n"
apt-get -qq update
apt-get install -y \
  curl \
  apt-transport-https \
	software-properties-common \
	debhelper \
	git-core < /dev/null > /dev/null
if [ ! -f /etc/apt/sources.list.d/ondrej-"${OS}"-nginx-"${DIST}".list ]; then
  case "${OS}" in
    "debian")
      curl -Lo /etc/apt/trusted.gpg.d/nginx.gpg https://packages.sury.org/nginx-mainline/apt.gpg
      echo "deb https://packages.sury.org/nginx-mainline/ ${DIST} main" > /etc/apt/sources.list.d/ondrej-"${OS}"-nginx-mainline-"${DIST}".list
      echo "deb-src https://packages.sury.org/nginx-mainline/ ${DIST} main" >> /etc/apt/sources.list.d/ondrej-"${OS}"-nginx-mainline-"${DIST}".list
    ;;
    "ubuntu")
      LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/nginx-mainline
      sed -i 's,^# deb-src,deb-src,' /etc/apt/sources.list.d/ondrej-"${OS}"-nginx-mainline-"${DIST}".list
    ;;
  esac
fi
apt-get -qq update
apt-get -qqy build-dep nginx < /dev/null > /dev/null
apt-get source nginx
CURPWD="$PWD"
cd nginx-*
printf "\e[0;36mPatching files ...\e[0m\n"
grep http-brotli debian/rules >/dev/null 2>&1 || patch -p1 < "${SCRIPT_DIR}"/rules.patch
grep http-brotli debian/control >/dev/null 2>&1 || patch -p1 < "${SCRIPT_DIR}"/control.patch
cp -pr "${SCRIPT_DIR}"/debian .
[ ! -d debian/modules/http-brotli-filter ] && git clone --recursive https://github.com/google/ngx_brotli debian/modules/http-brotli-filter
[ ! -s debian/modules/http-brotli-static ] && ln -s http-brotli-filter debian/modules/http-brotli-static
printf "\e[0;36mBuilding packages ...\e[0m\n"
dpkg-buildpackage -b -j"$(getconf _NPROCESSORS_ONLN)"
mkdir -p "${CI_PROJECT_DIR}/build-${DIST}/"
cp "$CURPWD"/*.deb "${CI_PROJECT_DIR}/build-${DIST}/"
